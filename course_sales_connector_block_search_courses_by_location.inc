<?php


class blockSearchCoursesLocation
        extends abstractBlock {



    /**
     *
     * @return string
     */
    public static function getInfoText() {
        return 'CourseSales.com - a block with a control for Location Sub Menus';

    }


    /**
     *
     * @return string
     */
    public static function getCacheType() {
        return DRUPAL_CACHE_PER_PAGE;

    }


    /**
     *
     * @return string
     */
    public static function getDelta() {
        return 'blockSearchCoursesLocation';

    }


    /**
     *
     * @return string
     */
    public function getTemplateName() {
        return 'search_courses_by_location';

    }


    /**
     *
     * @return string
     */
    public function getTemplatePath() {
        return 'search_courses_by_location';

    }


    /**
     *
     * @return array
     */
    public static function getTemplateVariables() {

        $result = array ( );

        $result[ 'text' ] = null;
        $result[ 'select_options' ] = null;
        $result[ 'objects' ] = null;

        return $result;

    }


    /**
     *
     * @param array $form
     */
    public function getConfigurationForm( array &$form ) {

        $this->addConfigurationTextField( $form );

    }


    /**
     *
     * @param array $edit
     */
    public function saveConfigurationForm( array $edit ) {

        $this->saveConfigurationVariable( $edit, self::blockText );

    }


    /**
     *
     * @param array $block
     */
    public function getContent( array &$block ) {

        global $csi;

        try {
            $csi->addSoapRequestActiveLocationNames();
            $csi->processRequest();
            $locations = $csi->getSoapDataActiveLocationNames();

            if ( !is_array( $locations ) ) {
                $locations = array();
            }

            $block[ 'title' ] = '';
            $block[ 'content' ] = theme( $this->buildTemplateName(),
                    array (
                'text' => variable_get( $this->getDelta() . self::blockText, '' ),
                'select_options' => $this->buildOptionSelect( $locations ),
                'objects' => $locations
                    ) );

// Catch
        } catch ( Exception $e ) {
            var_export( $e );
        }

    }

}
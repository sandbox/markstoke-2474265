<?php


class objectCourseDate
        extends abstractObject {
////////////////////////////////////////////////////////////////////////////////
//
//  VARIABLES
//
////////////////////////////////////////////////////////////////////////////////

    /**
     *
     * @var integer
     */
    protected $courseDateId;

    /**
     *
     * @var string
     */
    protected $dateStart;

    /**
     *
     * @var string
     */
    protected $dateEnd;

    /**
     *
     * @var integer
     */
    protected $venueId;

    /**
     *
     * @var string
     */
    protected $priceCurrent;

    /**
     *
     * @var string
     */
    protected $priceRRP;

    /**
     *
     * @var string
     */
    protected $priceMinimum;

    /**
     *
     * @var string
     */
    protected $priceResidential;

    /**
     *
     * @var integer
     */
    protected $placesTotal;

    /**
     *
     * @var integer
     */
    protected $enquiries;

    /**
     *
     * @var integer
     */
    protected $registrations;

    /**
     *
     * @var integer
     */
    protected $priceRuleId;

    /**
     *
     * @var integer
     */
    protected $priceRuleDate;

    /**
     *
     * @var integer
     */
    protected $priceRuleNextPrice;

    /**
     *
     * @var integer
     */
    protected $courseDescriptionId;

    /**
     *
     * @var integer
     */
    protected $statusId;

    /**
     *
     * @var string
     */
    protected $visibility;

    /**
     *
     * @var integer
     */
    protected $courseFormatId;

    /**
     *
     * @var integer
     */
    protected $currencyId;

    /**
     *
     * @var integer
     */
    protected $elapsedDays;

    /**
     *
     * @var integer
     */
    protected $courseCategoryId;

    /**
     *
     * @var string
     */
    protected $displayCategoryName;

    /**
     *
     * @var integer
     */
    protected $salesAgentId;

    /**
     *
     * @var integer
     */
    protected $residentialDescriptionId;

    /**
     *
     * @var integer
     */
    protected $providerId;

    /**
     *
     * @var string
     */
    protected $displayLocation;

    /**
     *
     * @var string
     */
    protected $locationId;

    /**
     *
     * @var string
     */
    protected $displayVenueName;

    /**
     *
     * @var string
     */
    protected $displayVenueUrl;

    /**
     *
     * @var string
     */
    protected $displayVenueLongitude;

    /**
     *
     * @var string
     */
    protected $displayVenueLatitude;

    /**
     *
     * @var string
     */
    protected $displayDescriptionShort;

    /**
     *
     * @var string
     */
    protected $displayDescriptionLong;

    /**
     *
     * @var string
     */
    protected $displayPriceCurrent;

    /**
     *
     * @var string
     */
    protected $displayPriceRrp;

    /**
     *
     * @var string
     */
    protected $displayPriceResidential;

    /**
     *
     * @var string
     */
    protected $displayPriceSaving;

    /**
     *
     * @var string
     */
    protected $displayPriceNext;

    /**
     *
     * @var string
     */
    protected $displayFormat;

    /**
     *
     * @var integer
     */
    protected $count;

    /**
     * An array of Step Ids and Form names
     *
     * @var array
     * @access protected
     */
    protected $displayDocumentForms;

    /**
     * Course Prices - current price
     *
     * @var float 11,2
     */
    protected $displayCoursePricesPriceCurrentCd;

    /**
     * Course Prices - current price currency Id
     *
     * @var integer
     */
    protected $displayCoursePricesCurrencyIdCd;

////////////////////////////////////////////////////////////////////////////////
//
//  STATIC - POSITION IN ARRAY
//
////////////////////////////////////////////////////////////////////////////////

    /**
     *
     * @var integer
     */

    const soapCourseDateId = 0;

    /**
     *
     * @var integer
     */

    const soapDateStart = 1;

    /**
     *
     * @var integer
     */

    const soapDateEnd = 2;

    /**
     *
     * @var integer
     */

    const soapVenueId = 3;

    /**
     *
     * @var integer
     */

    const soapPriceCurrent = 4;

    /**
     *
     * @var integer
     */

    const soapPriceRRP = 5;

    /**
     *
     * @var integer
     */

    const soapPriceMinimum = 6;

    /**
     *
     * @var integer
     */

    const soapPriceResidential = 7;

    /**
     *
     * @var integer
     */

    const soapPlacesTotal = 8;

    /**
     *
     * @var integer
     */

    const soapEnquiries = 9;

    /**
     *
     * @var integer
     */

    const soapRegistrations = 10;

    /**
     *
     * @var integer
     */

    const soapPriceRuleId = 11;

    /**
     *
     * @var integer
     */

    const soapPriceRuleDate = 12;

    /**
     *
     * @var integer
     */

    const soapPriceRuleNextPrice = 13;

    /**
     *
     * @var integer
     */

    const soapCourseDescriptionId = 14;

    /**
     *
     * @var integer
     */

    const soapStatusId = 15;

    /**
     *
     * @var integer
     */

    const soapVisibility = 16;

    /**
     *
     * @var integer
     */

    const soapCourseFormatId = 17;

    /**
     *
     * @var integer
     */

    const soapCurrencyId = 18;

    /**
     *
     * @var integer
     */

    const soapElapsedDays = 19;

    /**
     *
     * @var integer
     */

    const soapCourseCategoryId = 20;

    /**
     *
     * @var integer
     */

    const soapDisplayCategoryName = 21;

    /**
     *
     * @var integer
     */

    const soapSalesAgentId = 22;

    /**
     *
     * @var integer
     */

    const soapResidentialDescriptionId = 23;

    /**
     *
     * @var integer
     */

    const soapProviderId = 24;

    /**
     *
     * @var integer
     */

    const soapDisplayLocation = 25;

    /**
     *
     * @var integer
     */

    const soapDisplayVenueName = 26;

    /**
     *
     * @var integer
     */

    const soapDisplayVenueUrl = 27;

    /**
     *
     * @var integer
     */

    const soapDisplayVenueLongitude = 28;

    /**
     *
     * @var integer
     */

    const soapDisplayVenueLatitude = 29;

    /**
     *
     * @var integer
     */

    const soapDisplayDescriptionShort = 30;

    /**
     *
     * @var integer
     */

    const soapDisplayDescriptionLong = 31;

    /**
     *
     * @var integer
     */

    const soapDisplayPriceCurrent = 32;

    /**
     *
     * @var integer
     */

    const soapDisplayPriceRrp = 33;

    /**
     *
     * @var integer
     */

    const soapDisplayPriceResidential = 34;

    /**
     *
     * @var integer
     */

    const soapDisplayPriceSaving = 35;

    /**
     *
     * @var integer
     */

    const soapDisplayPriceNext = 36;

    /**
     *
     * @var integer
     */

    const soapDisplayFormat = 37;

    /**
     *
     * @var integer
     */

    const soapCount = 38;

    /**
     *
     * @var integer
     */

    const soapDisplayCoursePricePriceCurrent = 39;

    /**
     *
     * @var integer
     */

    const soapDisplayCoursePriceCurrencyId = 40;

    /**
     *
     * @var integer
     */

    const soapDisplayDocumentForms = 41;

    /**
     *
     * @var integer
     */

    const soapDisplayLocationId = 42;

////////////////////////////////////////////////////////////////////////////////
//
//  FUNCTIONS
//
////////////////////////////////////////////////////////////////////////////////


    /**
     *
     * @param array $data
     */
    function __construct( array &$data ) {

        $this->courseDateId = $data[ self::soapCourseDateId ];

        $this->dateStart = $data[ self::soapDateStart ];

        $this->dateEnd = $data[ self::soapDateEnd ];

        $this->venueId = $data[ self::soapVenueId ];

        $this->priceCurrent = $data[ self::soapPriceCurrent ];

        $this->priceRRP = $data[ self::soapPriceRRP ];

        $this->priceResidential = $data[ self::soapPriceResidential ];

        $this->placesTotal = $data[ self::soapPlacesTotal ];

        $this->enquiries = $data[ self::soapEnquiries ];

        $this->registrations = $data[ self::soapRegistrations ];

        $this->priceRuleId = $data[ self::soapPriceRuleId ];

        $this->priceRuleDate = $data[ self::soapPriceRuleDate ];

        $this->priceRuleNextPrice = $data[ self::soapPriceRuleNextPrice ];

        $this->courseDescriptionId = $data[ self::soapCourseDescriptionId ];

        $this->statusId = $data[ self::soapStatusId ];

        $this->visibility = $data[ self::soapVisibility ];

        $this->courseFormatId = $data[ self::soapCourseFormatId ];

        $this->currencyId = $data[ self::soapCurrencyId ];

        $this->elapsedDays = $data[ self::soapElapsedDays ];

        $this->courseCategoryId = $data[ self::soapCourseCategoryId ];

        $this->displayCategoryName = $data[ self::soapDisplayCategoryName ];

        $this->salesAgentId = $data[ self::soapSalesAgentId ];

        $this->residentialDescriptionId = $data[ self::soapResidentialDescriptionId ];

        $this->providerId = $data[ self::soapProviderId ];

        $this->displayLocation = $data[ self::soapDisplayLocation ];

        $this->displayVenueName = $data[ self::soapDisplayVenueName ];

        $this->displayVenueUrl = $data[ self::soapDisplayVenueUrl ];

        $this->displayVenueLongitude = $data[ self::soapDisplayVenueLongitude ];

        $this->displayVenueLatitude = $data[ self::soapDisplayVenueLatitude ];

        $this->displayDescriptionShort = $data[ self::soapDisplayDescriptionShort ];

        $this->displayDescriptionLong = $data[ self::soapDisplayDescriptionLong ];

        $this->displayPriceCurrent = $data[ self::soapDisplayPriceCurrent ];

        $this->displayPriceRrp = $data[ self::soapDisplayPriceRrp ];

        $this->displayPriceResidential = $data[ self::soapDisplayPriceResidential ];

        $this->displayPriceSaving = $data[ self::soapDisplayPriceSaving ];

        $this->displayPriceNext = $data[ self::soapDisplayPriceNext ];

        $this->displayFormat = $data[ self::soapDisplayFormat ];

        $this->count = $data[ self::soapCount ];

        $this->displayDocumentForms = $data[ self::soapDisplayDocumentForms ];

        $this->displayCoursePricesCurrencyIdCd = $data[ self::soapDisplayCoursePriceCurrencyId ];

        $this->displayCoursePricesPriceCurrentCd = $data[ self::soapDisplayCoursePricePriceCurrent ];

        $this->locationId = $data[ self::soapDisplayLocationId ];

    }

////////////////////////////////////////////////////////////////////////////////
//
//  GET
//
////////////////////////////////////////////////////////////////////////////////


    /**
     *
     * @return integer
     */
    public function getCourseDateId() {
        return $this->courseDateId;

    }


    /**
     *
     * @return string yyyy-mm-dd
     */
    public function getDateStart() {
        return $this->dateStart;

    }


    /**
     *
     * @return string
     */
    public function getDateStartFormatted( $format ) {
        return constants::getDateFormatted( $this->dateStart, $format );

    }


    /**
     *
     * @return string yyyy-mm-dd
     */
    public function getDateEnd() {
        return $this->dateEnd;

    }


    /**
     *
     * @return string
     */
    public function getDateEndFormatted( $format ) {
        return constants::getDateFormatted( $this->dateEnd, $format );

    }


    /**
     *
     * @return integer
     */
    public function getVenueId() {
        return $this->venueId;

    }


    /**
     *
     * @return string
     */
    public function getPriceCurrent() {
        return $this->priceCurrent;

    }


    /**
     *
     * @return string
     */
    public function getPriceRRP() {
        return $this->priceRRP;

    }


    /**
     *
     * @return string
     */
    public function getPriceMinimum() {
        return $this->priceMinimum;

    }


    /**
     *
     * @return string
     */
    public function getPriceResidential() {
        return $this->priceResidential;

    }


    /**
     *
     * @return integer
     */
    public function getPlacesTotal() {
        return $this->placesTotal;

    }


    /**
     *
     * @return integer
     */
    public function getEnquiries() {
        return $this->enquiries;

    }


    /**
     *
     * @return integer
     */
    public function getRegistrations() {
        return $this->registrations;

    }


    /**
     *
     * @return boolean
     */
    public function hasPlaces() {

        if ( empty( $this->placesTotal ) ) {
            return true;
        }

        if ( $this->registrations >= $this->placesTotal ) {
            return false;
        }

        return true;

    }


    /**
     * @return integer
     */
    public function getPercentFull() {

        if ( empty( $this->placesTotal ) ) {
            return 0;
        }

        return round( ( $this->registrations / $this->placesTotal ) * 100 );

    }


    /**
     *
     * @return integer
     */
    public function getPriceRuleId() {
        return $this->priceRuleId;

    }


    /**
     *
     * @return string
     */
    public function getPriceRuleDate() {
        return $this->priceRuleDate;

    }


    /**
     *
     * @return string
     */
    public function getPriceRuleNextPrice() {
        return $this->priceRuleNextPrice;

    }


    /**
     *
     * @return integer
     */
    public function getCourseDescriptionId() {
        return $this->courseDescriptionId;

    }


    /**
     *
     * @return integer
     */
    public function getStatusId() {
        return $this->statusId;

    }


    /**
     *
     * @return string
     */
    public function getVisibility() {
        return $this->visibility;

    }


    /**
     *
     * @return integer
     */
    public function getCourseFormatId() {
        return $this->courseFormatId;

    }


    /**
     *
     * @return integer
     */
    public function getCurrencyId() {
        return $this->currencyId;

    }


    /**
     *
     * @return integer
     */
    public function getElapsedDays() {
        return $this->elapsedDays;

    }


    /**
     *
     * @return integer
     */
    public function getCourseCategoryId() {
        return $this->courseCategoryId;

    }


    /**
     *
     * @return string
     */
    public function getDisplayCategoryName() {
        return $this->displayCategoryName;

    }


    /**
     *
     * @return integer
     */
    public function getSalesAgentId() {
        return $this->salesAgentId;

    }


    /**
     *
     * @return integer
     */
    public function getResidentialDescriptionId() {
        return $this->residentialDescriptionId;

    }


    /**
     *
     * @return integer
     */
    public function getProviderId() {
        return $this->providerId;

    }


    /**
     *
     * @return string
     */
    public function getDisplayLocation() {
        return $this->displayLocation;

    }


    /**
     *
     * @return integer
     */
    public function getLocationId() {
        return $this->locationId;

    }


    /**
     *
     * @return string
     */
    public function getDisplayVenueName() {
        return $this->displayVenueName;

    }


    /**
     *
     * @return string
     */
    public function getDisplayVenueUrl() {
        return $this->displayVenueUrl;

    }


    /**
     *
     * @return string
     */
    public function getDisplayVenueLongitude() {
        return $this->displayVenueLongitude;

    }


    /**
     *
     * @return string
     */
    public function getDisplayVenueLatitude() {
        return $this->displayVenueLatitude;

    }


    /**
     *
     * @return boolean
     */
    public function hasLocationUrl() {

        if ( empty( $this->displayVenueLongitude ) ) {
            return false;
        }

        if ( empty( $this->displayVenueLatitude ) ) {
            return false;
        }

        return true;

    }


    /**
     *
     * @return string
     */
    public function getDisplayDescriptionShort() {
        return $this->displayDescriptionShort;

    }


    /**
     *
     * @return string
     */
    public function getDisplayDescriptionLong() {
        return $this->displayDescriptionLong;

    }


    /**
     *
     * @return string
     */
    public function getDisplayPriceCurrent() {
        return $this->displayPriceCurrent;

    }


    /**
     *
     * @return string
     */
    public function getDisplayPriceRrp() {
        return $this->displayPriceRrp;

    }


    /**
     *
     * @return string
     */
    public function getDisplayPriceResidential() {
        return $this->displayPriceResidential;

    }


    /**
     *
     * @return string
     */
    public function getDisplayPriceSaving() {
        return $this->displayPriceSaving;

    }


    /**
     *
     * @return string
     */
    public function getDisplayPriceNext() {
        return $this->displayPriceNext;

    }


    /**
     *
     * @return string
     */
    public function getDisplayFormat() {
        return $this->displayFormat;

    }


    /**
     *
     * @return integer
     */
    public function getCount() {
        return $this->count;

    }


    /**
     *
     * @return array
     */
    public function getDisplayDocumentForms() {

        return $this->displayDocumentForms;

    }


    /**
     *
     * @return array
     */
    public function getDisplayDocumentFormUrls() {

        $result = array ( );

        if ( empty( $this->displayDocumentForms ) || !is_array( $this->displayDocumentForms ) ) {
            return $result;
        }

        $cd = constants::csCourseDateId;
        $cd .= '=';
        $cd .= $this->courseDateId;
        $cd .= '&';

        foreach ( $this->displayDocumentForms as $detail ) {
            if ( is_array( $detail ) ) {

                $link = array ( );
                $link[ 0 ] = $cd;
                $link[ 0 ] .= constants::csStepId;
                $link[ 0 ] .= '=';
                $link[ 0 ] .= $detail[ constants::csStepId ];
                $link[ 1 ] = $detail[ constants::csFormTitle ];

                $result[ ] = $link;
            }
        }

        return $result;

    }


    /**
     *
     * @return array
     */
    public function getDisplayDocumentFormFields() {

        $result = array ( );

        if ( empty( $this->displayDocumentForms ) || !is_array( $this->displayDocumentForms ) ) {
            return $result;
        }

        $cd = '<input type="hidden" name="';
        $cd .= constants::csCourseDateId;
        $cd .= '" value="';
        $cd .= $this->courseDateId;
        $cd .= '" />';

        foreach ( $this->displayDocumentForms as $detail ) {
            if ( is_array( $detail ) ) {

                $link = array ( );
                $link[ 0 ] = $cd;
                $link[ 0 ] .= '<input type="hidden" name="';
                $link[ 0 ] .= constants::csStepId;
                $link[ 0 ] .= '" value="';
                $link[ 0 ] .= $detail[ constants::csStepId ];
                $link[ 0 ] .= '" />';
                $link[ 1 ] = $detail[ constants::csFormTitle ];

                $result[ ] = $link;
            }
        }

        return $result;

    }


    /**
     *
     * @return string
     */
    public function getDisplayCoursePricesPriceCurrentCd() {
        return $this->displayCoursePricesPriceCurrentCd;

    }


    /**
     *
     * @return string
     */
    public function getDisplayCoursePricesCurrencyIdCd() {
        return $this->displayCoursePricesCurrencyIdCd;

    }

}
<?php


class blockCourseListDetailByLocation
        extends abstractBlockCourseList {


    /**
     *
     * @return string
     */
    public static function getInfoText() {
        return 'CourseSales.com - a block showing a listing of courses by location';

    }


    /**
     *
     * @return string
     */
    public static function getCacheType() {
        return DRUPAL_CACHE_PER_PAGE;

    }


    /**
     *
     * @return string
     */
    public static function getDelta() {
        return 'blockCourseListDetailByLocation';

    }


    /**
     *
     * @return string
     */
    public function getTemplateName() {
        return 'course_list_detail_by_location';

    }


    /**
     *
     * @return string
     */
    public function getTemplatePath() {
        return 'course_list_detail_by_location';

    }

}
<?php


class blockFormDisplay
        extends abstractBlockCourseList {


    /**
     *
     * @return string
     */
    public static function getInfoText() {
        return 'CourseSales.com - a block that shows a form';

    }


    /**
     *
     * @return string
     */
    public static function getCacheType() {
        return DRUPAL_CACHE_PER_PAGE;

    }


    /**
     *
     * @return string
     */
    public static function getDelta() {
        return 'blockFormDisplay';

    }


    /**
     *
     * @return string
     */
    public function getTemplateName() {
        return 'form_display';

    }


    /**
     *
     * @return string
     */
    public function getTemplatePath() {
        return 'form_display';

    }


}
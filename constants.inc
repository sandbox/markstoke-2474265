<?php


class constants {

    /**
     * 
     */
    const pathForm = 'csPthFrm';

    const pathFormPost = 'csPthFrmPost';

    const protocolRest = 1;

    const protocolSoap = 2;

    const protocolPublicPages = 3;

////////////////////////////////////////////////////////////////////////////////
//  CONFIGURATION
////////////////////////////////////////////////////////////////////////////////

    const moduleName = 'course_sales_connector';

    const moduleCfgUri = 'course_sales_connector_uri';

    const moduleCfgUsername = 'course_sales_connector_username';

    const moduleCfgPassword = 'course_sales_connector_password';

    const moduleCfgProtocol = 'course_sales_connector_protocol';

    const moduleCfgEmail = 'course_sales_connector_email';

////////////////////////////////////////////////////////////////////////////////
//  CONFIGURATION
////////////////////////////////////////////////////////////////////////////////

    const csMode = 'csMode';

    const csModeGetForm = 'csModeGetForm';

////////////////////////////////////////////////////////////////////////////////
//  BLOCKS
////////////////////////////////////////////////////////////////////////////////

    const blockDeltaCoursesByType = 'blockCoursesByType';

    const blockDeltaCoursesByLocation = 'blockCoursesByLocation';

    const blockDeltaCoursesByProfession = 'blockCoursesByProfession';

    const blockDeltaCourseSummary = 'blockCourseSummary';

    const blockFormListSize = 'ListSize';

    const blockFormWidgetType = 'WidgetType';

    const blockFormWidgetTypeSelect = 'WidgetTypeSelect';

    const blockFormWidgetTypeAutocomplete = 'WidgetTypeAutocomplete';

////////////////////////////////////////////////////////////////////////////////
//  OPTIONS
////////////////////////////////////////////////////////////////////////////////

    /**
     * CourseSales.com REST parameter representing Options
     *
     * @static
     * @var string
     */
    const csOptions = 'Options';

    /**
     * CourseSales.com REST parameter representing the Option web service
     * request type
     *
     * @static
     * @var string
     */
    const csOptionType = 'OptionType';

    /**
     * CourseSales.com REST parameter representing the Option web service
     * depth
     *
     * @static
     * @var string
     */
    const csOptionDepth = 'OptionDepth';

    /**
     * CourseSales.com REST parameter representing the Option web service
     * request type
     *
     * @static
     * @var string
     */
    const csOptionMeta = 'Options meta';

    /**
     * CourseSales.com REST parameter representing the Option web service
     * level of detail request key
     *
     * @static
     * @var string
     */
    const csOptionSoapDetail = 'OptionSoapDetail';

    /**
     * CourseSales.com REST parameter representing the Option web service
     * level of detail request for all data
     *
     * @static
     * @var string
     */
    const csOptionSoapDetailAll = 'All';

    /**
     * CourseSales.com REST parameter representing the Option web service
     * level of detail request for all data
     *
     * @static
     * @var string
     */
    const csOptionSoapDetailNames = 'Names';

    /**
     * CourseSales.com REST parameter representing the Option Type
     * Course Category
     *
     * @static
     * @var string
     */
    const csOptionCourseCategory = 'Course Category';

    /**
     * CourseSales.com REST parameter representing the Option Type
     * Course Location
     *
     * @static
     * @var string
     */
    const csOptionCourseLocation = 'Course Location';

    /**
     * CourseSales.com REST parameter representing the Option Type
     * Web Site Index
     *
     * @static
     * @var string
     */
    const csOptionWebSiteIndex = 'Web Site Index';

    /**
     * CourseSales.com REST parameter representing the Option Type
     * Extension
     *
     * @static
     * @var string
     */
    const csOptionExtension = 'Extension';

    /**
     * CourseSales.com REST parameter representing the Option Type
     * Extension
     *
     * @static
     * @var string
     */
    const csOptionId = 'OptionId';

    /**
     * CourseSales.com REST parameter representing the Option Type
     * Extension
     *
     * @static
     * @var string
     */
    const csOptionName = 'OptionName';


////////////////////////////////////////////////////////////////////////////////
//  COURSE DATES
////////////////////////////////////////////////////////////////////////////////

    /**
     * CourseSales.com REST parameter representing Course Dates
     *
     * @static
     * @var string
     */
    const csCourse = 'Course Dates';

    /**
     * CourseSales.com REST parameter representing the Course Date web service
     * request type
     *
     * @static
     * @var string
     */
    const csCourseType = 'CourseDateType';

    /**
     * CourseSales.com REST parameter representing the Course Date Type
     * Course List
     *
     * @static
     * @var string
     */
    const csCourseTypeList = 'CourseDateList';

    /**
     * CourseSales.com REST parameter representing the Course Date Type
     * Course List
     *
     * @static
     * @var string
     */
    const csCourseTypeListMeta = 'CourseDateList meta';

    /**
     * CourseSales.com REST parameter representing the Course Date Type
     * Course List
     *
     * @static
     * @var string
     */
    const csCourseTypeListCount = 'CourseDateList count';

    /**
     * CourseSales.com REST parameter representing the Course Date Type
     * Course List
     *
     * @static
     * @var string
     */
    const csCourseTypeMap = 'CourseDateMap';

    /**
     * CourseSales.com REST parameter representing the Course Date Type
     * Course List
     *
     * @static
     * @var string
     */
    const csCourseTypeMapMeta = 'CourseDateMap meta';

    /**
     * CourseSales.com REST parameter representing the Course Date Type
     * Course List
     *
     * @static
     * @var string
     */
    const csCourseTypeFiles = 'CourseDateFiles';

    /**
     * CourseSales.com REST parameter representing the Course Date Type
     * Course List
     *
     * @static
     * @var string
     */
    const csCourseTypeFilesMeta = 'CourseDateFiles meta';

    /**
     * CourseSales.com REST parameter representing the Course Date Type
     * Course List
     *
     * @static
     * @var string
     */
    const csCourseTypeListNextId = 'CourseDateListNextId';

    /**
     * CourseSales.com REST parameter representing the Course Date web service
     * request type
     *
     * @static
     * @var string
     */
    const csCourseDateId = 'CourseDateId';

    /**
     * CourseSales.com REST parameter representing the Course Date web service
     * request type
     *
     * @static
     * @var string
     */
    const csCourseDateRequestActiveYearMonth = 'ActiveYearMonth';

    /**
     * CourseSales.com REST parameter representing the Course Date web service
     * request type
     *
     * @static
     * @var string
     */
    const csCourseDateRequestLocationCheapest = 'CourseDateLocationCheapest';

    /**
     * CourseSales.com REST parameter representing the Course Date web service
     * request type
     *
     * @static
     * @var string
     */
    const csCourseDateRequestLocationNext = 'CourseDateLocationNext';

    /**
     * CourseSales.com REST parameter representing the Course Date web service
     * request type
     *
     * @static
     * @var string
     */
    const csCourseDateRequestDiscount = 'CourseDateDiscount';


////////////////////////////////////////////////////////////////////////////////
//  PROCESS STEP
////////////////////////////////////////////////////////////////////////////////

    /**
     * CourseSales.com REST parameter representing Venue
     *
     * @static
     * @var string
     */
    const csStepId = 'ProcessStepID';


////////////////////////////////////////////////////////////////////////////////
//  VENUE
////////////////////////////////////////////////////////////////////////////////

    /**
     * CourseSales.com REST parameter representing Venue
     *
     * @static
     * @var string
     */
    const csVenue = 'Venues';


////////////////////////////////////////////////////////////////////////////////
//  TAX
////////////////////////////////////////////////////////////////////////////////

    /**
     * CourseSales.com REST parameter representing Tax
     *
     * @static
     * @var string
     */
    const csTax = 'Tax Rules';

    /**
     * CourseSales.com REST parameter representing Tax
     *
     * @static
     * @var string
     */
    const csPrice = 'Price';


////////////////////////////////////////////////////////////////////////////////
//  CURRENCIES
////////////////////////////////////////////////////////////////////////////////

    /**
     * CourseSales.com REST parameter representing Currencies
     *
     * @static
     * @var string
     */
    const csCurrencies = 'Currencies';


////////////////////////////////////////////////////////////////////////////////
//  CONTENT
////////////////////////////////////////////////////////////////////////////////

    /**
     * CourseSales.com REST parameter representing Content
     *
     * @static
     * @var string
     */
    const csContent = 'Content';

    /**
     * CourseSales.com REST parameter representing the Content web service
     * request type
     *
     * @static
     * @var string
     */
    const csContentType = 'ContentType';

    /**
     * CourseSales.com REST parameter representing the Content Type
     * Web Index
     *
     * @static
     * @var string
     */
    const csContentWebIndex = 'ContentWebIndex';

    /**
     * CourseSales.com REST parameter representing the Content
     * meta data
     *
     * @static
     * @var string
     */
    const csContentMeta = 'Content meta';


////////////////////////////////////////////////////////////////////////////////
//  FORMS
////////////////////////////////////////////////////////////////////////////////

    /**
     * CourseSales.com REST parameter representing Forms
     *
     * @static
     * @var string
     */
    const csForms = 'Forms';

    /**
     * CourseSales.com REST parameter representing the Form web service
     * request type
     *
     * @static
     * @var string
     */
    const csFormType = 'FormType';

    /**
     * CourseSales.com REST parameter representing the Form web service
     * request type
     *
     * @static
     * @var string
     */
    const csFormTypeHtml = 'HTML';

    /**
     * CourseSales.com REST parameter representing the Form web service
     * request type
     *
     * @static
     * @var string
     */
    const csFormTypePayment = 'Payment';

    /**
     * CourseSales.com REST parameter representing the Form web service
     * request type
     *
     * @static
     * @var string
     */
    const csFormTypePaymentMeta = 'Payment meta';

    /**
     * CourseSales.com REST parameter representing the Form web service
     * request type
     *
     * @static
     * @var string
     */
    const csFormTypePost = 'PostNewForm';

    /**
     * CourseSales.com REST parameter representing the Form web service
     * request type
     *
     * @static
     * @var string
     */
    const csFormTitle = 'FormTitle';


////////////////////////////////////////////////////////////////////////////////
//  DOCUMENTS
////////////////////////////////////////////////////////////////////////////////

    /**
     * CourseSales.com REST parameter representing Documents
     *
     * @static
     * @var string
     */
    const csDocuments = 'Documents';

    /**
     * CourseSales.com REST parameter representing the Document web service
     * request type
     *
     * @static
     * @var string
     */
    const csDocumentType = 'DocumentRequestType';

    /**
     * CourseSales.com REST parameter representing the Document Type
     * that returns a list of CourseSales.com Contacts for a course
     *
     * @static
     * @var string
     */
    const csDocumentTypeCourseDateContactsInternal = 'CourseDateContactsInternal';

    /**
     * CourseSales.com REST parameter representing Documents
     *
     * @static
     * @var string
     */
    const csDocumentCourseDateId = 'DocumentCourseDateId';

    /**
     * CourseSales.com REST parameter representing Documents
     *
     * @static
     * @var string
     */
    const csDocumentContactId = 'DocumentContactId';

    /**
     * CourseSales.com REST parameter representing Documents
     *
     * @static
     * @var string
     */
    const csDocumentStatusRegistration = 'DocumentStatusRegistration';

    /**
     * CourseSales.com REST parameter representing Documents
     *
     * @static
     * @var string
     */
    const csDocumentStatusRegistered = 'Registered';

    /**
     * CourseSales.com REST parameter representing Documents
     *
     * @static
     * @var string
     */
    const csDocumentFormId = 'DocumentFormId';

    /**
     * CourseSales.com REST parameter representing Documents
     *
     * @static
     * @var string
     */
    const csDocumentSource = 'DocumentSource';


////////////////////////////////////////////////////////////////////////////////
//  CONTACTS
////////////////////////////////////////////////////////////////////////////////

    /**
     * CourseSales.com REST parameter representing Contacts
     *
     * @static
     * @var string
     */
    const csContacts = 'Contacts';

    /**
     * CourseSales.com REST parameter representing Contacts
     *
     * @static
     * @var string
     */
    const csContactRequestType = 'ContactRequestType';

    /**
     * CourseSales.com REST parameter representing Contacts
     *
     * @static
     * @var string
     */
    const csContactRequestTypeLogin = 'ContactLogin';

    /**
     * CourseSales.com REST parameter representing Contacts
     *
     * @static
     * @var string
     */
    const csContactRequestTypeContact = 'Contact';

    /**
     * CourseSales.com REST parameter representing Contacts
     *
     * @static
     * @var string
     */
    const csContactRequestTypeContactCourses = 'Courses';


////////////////////////////////////////////////////////////////////////////////
//  STATUS
////////////////////////////////////////////////////////////////////////////////

    /**
     *
     * @var string
     */
    const csStatus = 'Status';

    /**
     *
     * @var string
     */
    const csStatusTableName = 'StatusTableName';

    /**
     *
     * @var string
     */
    const csStatusStepId = 'StatusStepId';


////////////////////////////////////////////////////////////////////////////////
//  TABLE LINKS
////////////////////////////////////////////////////////////////////////////////

    /**
     *
     * @var string
     */
    const csTableLinks = 'Table Links';

    /**
     *
     * @var string
     */
    const csTableLinkType = 'TableLinkType';

    /**
     *
     * @var string
     */
    const csTableLinkTypeLinks = 'Links';

    /**
     *
     * @var string
     */
    const csTableLinkFromName = 'TableLinkFromName';

    /**
     *
     * @var string
     */
    const csTableLinkFromKey = 'TableLinkFromKey';

    /**
     *
     * @var string
     */
    const csTableLinkFromCategory = 'TableLinkFromCategory';

    /**
     *
     * @var string
     */
    const csTableLinkToName = 'TableLinkToName';

    /**
     *
     * @var string
     */
    const csTableLinkToType = 'TableLinkToType';

    /**
     *
     * @var string
     */
    const csTableLinkMeta = 'Table Links meta';

////////////////////////////////////////////////////////////////////////////////
//  COMMON
////////////////////////////////////////////////////////////////////////////////

    /**
     * CourseSales.com REST parameter representing the page number to return
     *
     * @static
     * @var string
     */
    const csHttpHost = 'HTTP_HOST';

    /**
     * CourseSales.com REST parameter representing the page number to return
     *
     * @static
     * @var string
     */
    const csRequestPage = 'Page';

    /**
     * CourseSales.com REST parameter representing the number of items per
     * page to return
     *
     * @static
     * @var string
     */
    const csRequestPageLimit = 'PageItemLimit';

    /**
     * CourseSales.com REST parameter representing the page number to return
     *
     * @static
     * @var string
     */
    const csRequestServiceType = 'ServiceType';

    /**
     * CourseSales.com REST parameter representing the page number to return
     *
     * @static
     * @var string
     */
    const csRequestServiceMoodleCourseDateId = 'Moodle Course Date Id';

    /**
     * CourseSales.com REST parameter representing a request to log the query
     * in the CourseSales.com log
     *
     * @static
     * @var string
     */
    const csRequestDebug = 'debug';

    /**
     * CourseSales.com REST parameter representing a request to log the query
     * in the CourseSales.com log
     *
     * @static
     * @var string
     */
    const csRequestDebugOn = 'on';

    /**
     * CourseSales.com REST parameter representing the web services username
     *
     * @static
     * @var string
     */
    const csLoginUser = 'LoginUsername';

    /**
     * CourseSales.com REST parameter representing the web services password
     *
     * @static
     * @var string
     */
    const csLoginPw = 'LoginPassword';

    /**
     * CourseSales.com Course array index for the Contact External Id
     *
     * @static
     * @var string
     */
    const csContactId = 'ContactId';

    /**
     * CourseSales.com Course array index for the Contact External Id
     *
     * @static
     * @var string
     */
    const csContactExternalId = 'ContactExternalId';

    /**
     * CourseSales.com Course array index for the Contact External Id
     *
     * @static
     * @var string
     */
    const csContactNameFirst = 'ContactNameFirst';

    /**
     * CourseSales.com Course array index for the Contact External Id
     *
     * @static
     * @var string
     */
    const csContactNameSurname = 'ContactNameSurname';

    /**
     * CourseSales.com Course array index for the Contact External Id
     *
     * @static
     * @var string
     */
    const csContactEmail = 'ContactEmail';

    /**
     * CourseSales.com Course array index for the Contact External Id
     *
     * @static
     * @var string
     */
    const csContactMobile = 'ContactMobile';

    /**
     * CourseSales.com REST parameter representing Contacts
     *
     * @static
     * @var string
     */
    const csContactPassword = 'ContactPassword';

    /**
     *
     * @var string
     */
    const dateFormat = 'dateFormat';


    /**
     *
     * @param string $sqlDate
     * @param integer $formatId
     */
    public static function getDateFormatted( $sqlDate, $format ) {

        return date_format( date_create( $sqlDate ), $format );

    }

}
<?php


class blockCourseListSummary
        extends abstractBlockCourseList {

    

    /**
     *
     * @return string
     */
    public static function getInfoText() {
        return 'CourseSales.com - a block showing a summary listing of courses';

    }


    /**
     *
     * @return string
     */
    public static function getCacheType() {
        return DRUPAL_CACHE_PER_PAGE;

    }


    /**
     *
     * @return string
     */
    public static function getDelta() {
        return 'blockCourseSummary';

    }


    /**
     *
     * @return string
     */
    public function getTemplateName() {
        return 'course_list_summary';

    }


    /**
     *
     * @return string
     */
    public function getTemplatePath() {
        return 'course_list_summary';

    }


    /**
     *
     */
    protected function setupRequestCourses() {
        $this->setupRequestCourseLocationNext();

    }


    /**
     *
     * @return array
     */
    protected function buildCourses() {
        return $this->buildCoursesLocationNext();

    }

}
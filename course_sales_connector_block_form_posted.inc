<?php


class blockFormPosted
        extends abstractBlockCourseList {


    /**
     *
     * @return string
     */
    public static function getInfoText() {
        return 'CourseSales.com - a block that shows after a form has been posted';

    }


    /**
     *
     * @return string
     */
    public static function getCacheType() {
        return DRUPAL_CACHE_PER_PAGE;

    }


    /**
     *
     * @return string
     */
    public static function getDelta() {
        return 'blockFormPosted';

    }


    /**
     *
     * @return string
     */
    public function getTemplateName() {
        return 'form_posted';

    }


    /**
     *
     * @return string
     */
    public function getTemplatePath() {
        return 'form_posted';

    }


}
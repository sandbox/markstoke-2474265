<?php
/**
  - Course List for Desktop and Mobile
  - Course Results Count
  - Paginations
  - Header Icons
  - More Details etc.
 */
?>

<div><?php print $text ?></div>

<?php
// Global Variables;

$per_page = sizeof( $courses );  // Items per page

if ( isset( $_GET[ 'Page' ] ) ) {  // Check page found or not
    $page = $_GET[ 'Page' ];
} else {
    $page = 1;
}

$next_page = $page + 1;
$prev_page = $page - 1;

$courseLocationId = $_GET[ 'CourseLocationId' ];
?>


<!-- Course Listing -->

<?php if ( !isset( $_GET[ 'ProcessStepID' ] ) ): ?>
    <?php if ( isset( $_GET[ 'CourseLocationId' ] ) ): ?>
        <div class="row searchpagi">
            <!-- Count Results-->
            <div class="col-sm-6">
                    <div class="search_result">We found <span class="counts"><?php print $countPages; ?></span> listings.</div>
            </div>
            <!-- End Count Results-->

            <!-- Pagination for desktop -->
            <div class="col-sm-6">
                <nav class="pgmargin">
        <?php
        if ( isset( $_GET[ 'CourseLocationId' ] ) ) {
            if ( $countPages > $per_page ) {
                echo "<div class='pagination'>";

                // Previous link
                if ( $_GET[ 'page' ] > 1 and !empty( $_GET[ 'page' ] ) ) {

                    if ( !isset( $_GET[ 'CourseLocationId' ] ) ) {

                        print "<li><a class='jp-previous' href='?q=courses&page=$prev_page'>" . '&lt; Prev' . "</a></li>";
                    } else {
                        print "<li><a class='jp-previous' href='?q=courses&CourseLocationId=" . $courseLocationId . "&page=$prev_page'>" . '&lt; Prev' . "</a></li>";
                    }
                } else {
                    print "<li><a class='jp-previous' href=''>" . '&lt; Prev' . "</a></li>";
                }

                // Page numbers
                for ( $p = 1; $p <= $countPages; $p++ ) {
                    if ( !isset( $_GET[ 'CourseLocationId' ] ) ) {
                        echo "<li><a href='?q=courses&page=" . $p . "'>" . $p . "</a></li>";
                    } else {
                        echo "<li><a href='?q=courses&CourseLocationId=" . $courseLocationId . "&page=" . $p . "'>" . $p . "</a></li>";
                    }
                }


                // Next link
                if ( $_GET[ 'page' ] == $countPages ) {
                    echo "<li><a class='jp-next' href=''>" . 'Next &gt;' . "</a></li>";
                } else {
                    if ( !isset( $_GET[ 'CourseLocationId' ] ) ) {
                        echo "<li><a class='jp-next' href='?q=courses&page=$next_page'>" . 'Next &gt;' . "</a></li>";
                    } else {
                        echo "<li><a class='jp-next' href='?q=courses&CourseLocationId=" . $courseLocationId . "&Page=$next_page'>" . 'Next &gt;' . "</a></li>";
                    }
                }


                print "</div>";
            }
        }
        ?>
                </nav>
            </div>
            <!-- End Pagination for desktop -->
        </div>
    <?php endif; ?>
<?php endif; ?>



<?php
if ( !empty( $courses ) && is_array( $courses ) ) {

    if ( !isset( $_GET[ 'ProcessStepID' ] ) ) {

        if ( isset( $_GET[ 'CourseLocationId' ] ) ) {

            $i = 0;

            print '<div class="upcoming_course" id="itemContainer">';

            foreach ( $courses as $course ) {
                if ( $course instanceof objectCourseDate ) {
                    ?>

                    <div class="sc_cover_sm">

                        <!-- Mobile Course list-->
                        <div class="mcbg row">
                            <h2 class="mctitle col-xs-12"><?php print $course->getDisplayCategoryName(); ?></h2>
                            <div class="col-xs-12">
                                <div class="msdate"><?php print $course->getDateStartFormatted( $dateFormat ) . ' to ' . $course->getDateEndFormatted( $dateFormat ); ?></div>
                                <h3 class="search_body_title"><?php print $course->getDisplayLocation(); ?></h3>
                                <h3 class="search_body_title">Description</h3>
                                <p><?php print $course->getDisplayDescriptionShort(); ?></p>
                    <?php
                    /* Get Random Id */
                    $courseId1 = explode( " ", $course->getDisplayLocation() );
                    $courseId = strtolower( implode( "-", $courseId1 ) ) . "mob" . $i;
                    ?>

                                <?php
                                $long_desc = $course->getDisplayDescriptionLong();
                                if ( !empty( $long_desc ) ):
                                    ?>
                                    <a class="mored" data-toggle="collapse" data-parent="#accordion1" href="#<?php print $courseId; ?>">More Details</a>
                                <?php endif; ?>

                                <div id="<?php print $courseId; ?>" class="panel-collapse collapse">
                                    <div class="details"><?php print $course->getDisplayDescriptionLong(); ?></div>
                                </div>

                                <div class="msoptions"> <a href="" class="mc_location">View course location</a> <a href="" class="mc_view">View course website</a> <a href="" class="mc_place">Places available</a>
                                    <div class="scprice"> NZD <strong><?php print $course->getDisplayPriceCurrent(); ?></strong> <span>(exc GST)</span></div>
                    <?php
                    // Inquiry and registration Button
                    print '<div>';
                    foreach ( $course->getDisplayDocumentFormUrls() as $form ) {
                        print '<a href="';
                        print $blockPathForm;
                        print '?' . $form[ 0 ] . '" class="searchbtn colorblue"/>' . $form[ 1 ] . '</a>';
                    }
                    print '</div>';
                    ?>
                                </div>
                            </div>
                        </div>
                        <!-- End Mobile Course list-->
                    </div>

                    <!-- Desktop Course list-->
                    <div class="sc_cover_lg">
                        <!-- List Header-->
                        <div class="row search_head">
                            <div class="col-sm-4 sctitle">Course Title <span><?php print $course->getDisplayCategoryName(); ?></span></div>
                            <!-- FROM MARK - THE ANCHOR FOR THE LOCATION WILL BE-->
                    <?php
                    $venueUrl = $course->getDisplayVenueUrl();
                    $lcoationUrl = $course->hasLocationUrl();

                    if ( !empty( $lcoationUrl ) ) {
                        $locHtml = '<div class="col-sm-2 sclocation">Location<br>';
                        $locHtml .= '<a target="_blank" href="https://www.google.com/maps/place/';
                        $locHtml .= $course->getDisplayVenueName();
                        $locHtml .= '/@';
                        $locHtml .= $course->getDisplayVenueLatitude();
                        $locHtml .= ',';
                        $locHtml .= $course->getDisplayVenueLongitude();
                        $locHtml .= ',17z/data=!3m1!4b1!4m2!3m1!1s0x6b12ae3becd5d3d9:0x519a987f7ac83ec5?hl=en">';
                        $locHtml .= 'ICON<a/>';
                        $locHtml .= '</div>';
                        print $locHtml;
                    } else { //  If location URL is empty
                        $locHtml = '<div class="col-sm-2 sclocation">';
                        $locHtml .= '</div>';
                        print $locHtml;
                    }
                    ?>
                            <!-- FROM MARK - THE ANCHOR FOR THE VENUE WILL BE-->
                            <?php
                            if ( !empty( $venueUrl ) ) {
                                print '<div class="col-sm-2 scvanue">Venue website<br>
                                <a href="' . $venueUrl . '" target="_blank">ICON<a/></div>';
                            } else { //  If venue URL is empty
                                print '<div class="col-sm-2 scvanue">
                                </div>';
                            }
                            ?>
                            <div class="col-sm-2 scplace">Places availableX<br>
                            <?php
                            if ( $course->hasPlaces() ) {
                                if ( $course->getPercentFull() < 80 ) {
                                    // TICK FOR PLACES AVAILABLE
                                    print '<div class="available" href="#" target="_blank">ICON</div>';
                                } else {
                                    // ICON FOR COURSE NEARLY FULL
                                    print '<div class="limited" href="#" target="_blank">ICON</div>';
                                }
                            } else {
                                // ICON FOR COURSE FULL
                                print '<div class="full" href="#" target="_blank">ICON</div>';
                            }
                            ?>
                            </div>
                            <div class="col-sm-2 scdate">date<span><?php print $course->getDateStartFormatted( $dateFormat ) . ' to <br>' . $course->getDateEndFormatted( $dateFormat ); ?></span></div>
                        </div>
                        <!-- End List Header-->

                        <!--Course List Body-->
                        <div class="row search_body">
                            <div class="col-sm-8 sbcontent">
                                <h3 class="search_body_title">Description</h3>
                                <p><?php print $course->getDisplayDescriptionShort(); ?></p>
                    <?php
                    /* Get Random Id */
                    $courseId1 = explode( " ", $course->getDisplayLocation() );
                    $courseId = strtolower( implode( "-", $courseId1 ) ) . $i;
                    ?>

                    <?php $long_desc
                            = $course->getDisplayDescriptionLong();
                    if ( !empty( $long_desc ) ):
                        ?>
                                    <a class="mored" data-toggle="collapse" data-parent="#accordion1" href="#<?php print $courseId; ?>">More Details</a>
                                <?php endif; ?>
                                <div id="<?php print $courseId; ?>" class="panel-collapse collapse">
                                    <div class="details"><?php print $course->getDisplayDescriptionLong(); ?></div>
                                </div>


                            </div>
                            <div class="col-sm-4 sbcontent">
                                <h3 class="search_body_title"><?php print $course->getDisplayLocation(); ?></h3>
                                <p><?php print $course->getDisplayVenueName(); ?></p>
                                <div class="scprice"><strong><?php print $course->getDisplayPriceCurrent(); ?></strong> <span>(exc GST)</span></div>
                    <?php
                    // Inquiry and registration Button
                    print '<div>';
                    foreach ( $course->getDisplayDocumentFormUrls() as $form ) {
                        print '<a href="';
                        print $blockPathForm;
                        print '?q=courses&' . $form[ 0 ] . '" class="searchbtn colorblue"/>' . $form[ 1 ] . '</a>';
                    }
                    print '</div>';
                    ?>
                            </div>
                        </div>

                        <!--End Course List Body-->

                    </div>
                    <!-- End Desktop Course list-->
                    <?php
                }
                $i++;
            }
            print '</div>';
        }
    }
}
?>




<?php if ( !isset( $_GET[ 'ProcessStepID' ] ) ): ?>
    <?php if ( isset( $_GET[ 'CourseLocationId' ] ) ): ?>
        <!-- Pagination for mobile -->
        <div class="sc_cover_sm">
            <div class="smpagination"> Showing page
                <p id="legend2"> </p>
                <div class="holder"> </div>
            </div>
        </div>
        <!-- End Pagination for mobile -->
        <div class="row downpg">
            <div class="col-sm-6 col-sm-offset-6">
                <nav class="pgmargin">
                    <!-- Pagination for desktop -->
        <?php
        if ( isset( $_GET[ 'CourseLocationId' ] ) ) {
            if ( $countPages > $per_page ) {
                echo "<div class='pagination'>";

                // Previous link
                if ( $_GET[ 'page' ] > 1 and !empty( $_GET[ 'page' ] ) ) {

                    if ( !isset( $_GET[ 'CourseLocationId' ] ) ) {

                        print "<li><a class='jp-previous' href='?q=courses&page=$prev_page'>" . '&lt; Prev' . "</a></li>";
                    } else {
                        print "<li><a class='jp-previous' href='?q=courses&CourseLocationId=" . $courseLocationId . "&page=$prev_page'>" . '&lt; Prev' . "</a></li>";
                    }
                } else {
                    print "<li><a class='jp-previous' href=''>" . '&lt; Prev' . "</a></li>";
                }

                // Page numbers
                for ( $p = 1; $p <= $countPages; $p++ ) {
                    if ( !isset( $_GET[ 'CourseLocationId' ] ) ) {
                        echo "<li><a href='?q=courses&page=" . $p . "'>" . $p . "</a></li>";
                    } else {
                        echo "<li><a href='?q=courses&CourseLocationId=" . $courseLocationId . "&page=" . $p . "'>" . $p . "</a></li>";
                    }
                }


                // Next link
                if ( $_GET[ 'page' ] == $countPages ) {
                    echo "<li><a class='jp-next' href=''>" . 'Next &gt;' . "</a></li>";
                } else {
                    if ( !isset( $_GET[ 'CourseLocationId' ] ) ) {
                        echo "<li><a class='jp-next' href='?q=courses&page=$next_page'>" . 'Next &gt;' . "</a></li>";
                    } else {
                        echo "<li><a class='jp-next' href='?q=courses&CourseLocationId=" . $courseLocationId . "&page=$next_page'>" . 'Next &gt;' . "</a></li>";
                    }
                }


                print "</div>";
            }
        }
        ?>
                    <!-- End Pagination for desktop -->
                </nav>
            </div>
        </div>
        <!-- End Course Listing -->
                <?php endif; ?>
            <?php endif; ?>


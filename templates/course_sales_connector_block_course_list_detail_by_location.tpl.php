<?php
/**
 */
?>
<div><?php print $text ?></div>
<div class="upcoming_locations">
  <div class="container">
    <div class="row">
    	
             
    
      <?php
        if ( ! empty( $courses ) && is_array( $courses ) ) {
			
			if(!isset($_GET['ProcessStepID'])){
			
			$f=0;
            foreach ( $courses as $course ) {
                if ( $course instanceof objectCourseDate ) {
					
					
					/*print "<pre>";
					print_r($course);
					print "<pre>";*/
					
					
					if($f==0){
						if(isset($_GET['CourseLocationId'])){
							print '<div class="uctitle">Upcoming courses in all of <span class="location-name"></span></div>';
						}
					}
								
					?>
                      <div class="col-md-3 col-sm-4">
                        <h2 class="ucourse_title"><?php print $course->getDisplayLocation(); ?></h2>
                        <div class="ucmiddle"> <span class="ucdate"><?php print $course->getDateStartFormatted( $dateFormat ); ?> </span> <?php print $course->getDisplayCategoryName(); ?> </div>
                        <div class="ucbottom"><?php print $course->getDisplayPriceCurrent(); ?><span class="exc">(exc GST)</span>
                          <?php
                            // Inquiry and registration Button
                            print '<ul class="registration">';
                            foreach ( $course->getDisplayDocumentFormUrls() as $form ) {
                                print '<li><a href="';
                                print $blockPathForm;
                                print '?q=locations&' . $form[ 0 ] . '" />' . $form[ 1 ] . '</a></li>';
                            }
                            print '</ul>';
                            ?>
                        </div>
                        <a class="uclink" href="?q=courses&CourseCategoryId=<?php print $course->getcourseCategoryId();?>">View more like this in <?php print $course->getDisplayLocation(); ?> courses</a></div>
      			<?php
                }
				$f++;
    	        }
			}
        }
        ?>
    </div>
  </div>
</div>

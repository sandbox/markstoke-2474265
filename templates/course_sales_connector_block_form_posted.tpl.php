<?php
/**
 */
?>
<div class="container-inline">
    <div><?php print $text ?></div>
    <?php
    if ( !empty( $documentExternalId ) ) {
        print '<div>Thanks, please quote this reference in any correspondence with us:';
        print $documentExternalId;
        print '</div>';
    }
    ?>
</div>

<?php
/**
 * Available variables:
 * - $text - this is set on the module configuration page
 * - $select_options - this is a set of option tages, eg <option value="volvo">Volvo</option>
 */
?>

<div class="ttmenu">
  <div class="container">
    <ul class="tnav">
      <?php
        $optionTop = '';
        foreach ( $objects as $location ) {
            switch ( $location->getDepth() ) {

                case 1 :
                    $optionTop .= '<option value="';
                    $optionTop .= $location->getId();
                    $optionTop .= '">';
                    $optionTop .= $location->getName();
                    $optionTop .= '</option>';
					print "<li><a href='?q=locations&CourseLocationId=".$location->getId()."'>".$location->getName()."</a></li>";
                    break;

            }
			
        }
        ?>
    </ul>
  </div>
</div>


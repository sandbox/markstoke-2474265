<?php
/**
 * Available variables:
 * - $text - this is set on the module configuration page
 * - $select_options - this is a set of option tages, eg <option value="volvo">Volvo</option>
 */
?>

<div class="container-inline">
  <div><?php print $text ?></div>
  <form method="get" action="courses?">
    <div class="form-item-select">
      <select name="ExtensionId" id="">
        <?php print $select_options ?>
      </select>
    </div>
    <input type="submit" value="Search Courses" />
  </form>
</div>

<?php
/**
 * Available variables:
 * - $text - this is set on the module configuration page
 * - $select_options - this is a set of option tages, eg <option value="volvo">Volvo</option>
 */
?>
<div class="container-inline">
    <div><?php print $text ?></div>
    <form method="get" action="courses?">
        <?php
        $optionTop = '';
        $optionLookup = array ( );
        $currentParent = 0;
		$optionTop .= '<option>Select Courses by Type</option>';
        foreach ( $objects as $category ) {
            if ( $category->getDepth() == 1 ) {
                $currentParent = $category->getId();
				
                $optionTop .= '<option value="';
                $optionTop .= $currentParent;
                $optionTop .= '">';
                $optionTop .= $category->getName();
                $optionTop .= '</option>';
                $optionLookup[ $currentParent ] = array ( );

				$parent[] = $currentParent;  // Get parent category list;
							
            } else {
                $optionLookup[ $currentParent ][ $category->getId() ] = str_repeat( '-',
                                $category->getDepth() - 2 ) .' '. $category->getName();
            }
        }
        ?>
        <div style="display:none" id="CourseCategoryOptions">
            <?php
           	 	print json_encode( $optionLookup );
            ?>
        </div>
        
        <div class="form-item-select">
		<!-- Parent Category list-->
        <select name="CourseCategoryGroupId" id="CourseCategoryGroupId">
            <?php print $optionTop ?>
        </select>
        
        <?php
        /* Sub Category list */        
		$lenParentCat = sizeof($parent);

		for($i=0; $i<=$lenParentCat; $i++){
			$topSublist = $optionLookup[$parent[$i]];
			print '<div class="ui-select">';
				print '<select name="CourseCategoryId" id="CourseCategoryId" class="cat_'.$parent[$i].'" style="display:none;">';
				$optionBottom = '';
				foreach ( $topSublist as $id => $name ) {
					
					$optionBottom .= '<option value="';
					$optionBottom .= $id;
					$optionBottom .= '">';
					$optionBottom .= $name;
					$optionBottom .= '</option>';
				}
				print $optionBottom;
				print '</select>';
			print '</div>';
		}
		?>
        </div>
   
        <input type="submit" value="Search Courses" />
    </form>
</div>

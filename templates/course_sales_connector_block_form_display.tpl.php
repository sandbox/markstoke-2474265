<?php
/**
 */
?>

<div class="container-inline"> 
  <script>
    jQuery.ajax({
                    type: 'POST',
                    url: './course_sales_connector/ajax',
                    dataType: 'json',
                    data: 'parm1=good stuff'
                });
    </script>
  <div class="registration-form">
    <?php if ( !empty( $courses ) && is_array( $courses ) ) {
            foreach ( $courses as $course ) {
				if ( $course instanceof objectCourseDate ) {
					
					if(isset($_GET['ProcessStepID'])) {
					/* Single course details */
				?>
                    <div class="back"> <a class="notfixed csButton" href="?q=courses">Back</a> </div>
                    <div class="drwwdth drawerHead"> <span>Course</span> </div>
                    <div class="drwwdth drawer">
                      <div id="divCourseMasterCourseCategoryId" class="acI fldWd50 fld wdg">
                        <label for="CourseMasterCourseCategoryId">Course Category</label>
                        <p><?php print $course->getDisplayCategoryName(); ?></p>
                      </div>
                      <div id="divCourseMasterProviderId" class="acI fldWd50 fld wdg">
                        <label for="CourseMasterProviderId">Provider</label>
                        <p>CC Learning</p>
                      </div>
                      <div id="divCourseDateStart" class="fldWd50 fld wdg">
                        <label for="CourseDateStart">Start Date</label>
                        <p><?php print $course->getDateStartFormatted( $dateFormat); ?></p>
                      </div>
                      <div id="divCourseDateEnd" class="fldWd50 fld wdg">
                        <label for="CourseDateStart">End Date</label>
                        <p><?php print $course->getDateEndFormatted( $dateFormat ); ?></p>
                      </div>
                      <div id="divCourseDateVenueId" class="divCourseDateVenueId wdg">
                        <label for="CourseDateVenueId">Venue</label>
                        <p id="CourseDateVenueId"><?php print $course->getDisplayVenueName(); ?></p>
                      </div>
                    </div>
                    <?php } 
			}
		} 
	}
	?>
    <?php
		/* Regitration and enquiry form*/
        if ( !empty( $form ) ) {
        print '<div>';
        print $text;
        print '</div><form>';
        print $form;
		print '<div id="csFormBottomButtons">';
        print '<input type="hidden" name="CourseDateId" value="'.$parameters[constants::csCourseDateId].'" />';
        print '<input class="notfixed csButton" type="submit" value="Submit" />';
		print '</div>';
        print '</form>';
    }
    ?>
  </div>
</div>

<?php
/**
 * Available variables:
 * - $text - this is set on the module configuration page
 * - $select_options - this is a set of option tages, eg <option value="volvo">Volvo</option>
 */
?>




<div class="container-inline">
    <div><?php print $text ?></div>
    <form method="get" action="locations?">
        <?php
        $optionTop = '';
		$optionTop .= '<option>Browse courses by region</option>';
        foreach ( $objects as $location ) {
            switch ( $location->getDepth() ) {

                case 1 :
					
                    $optionTop .= '<option value="';
                   // $optionTop .= $location->getId();
				    $optionTop .= $location->getId();
                    $optionTop .= '">';
                    $optionTop .= $location->getName();
                    $optionTop .= '</option>';
                    break;

                case 2:
                    $optionTop .= '<option value="';
                    $optionTop .= $location->getId();
                    $optionTop .= '">';
                    $optionTop .= '-';
                    $optionTop .= $location->getName();
                    $optionTop .= '</option>';
                    break;
            }
        }
        ?>
        <select name="CourseLocationId" id="" class="locationselect">
        	
            <?php 
			print $optionTop ?>
        </select>
        <div style="display:none;">
      <input type="submit" value="Search Courses" />
    </div>
    </form>
</div>

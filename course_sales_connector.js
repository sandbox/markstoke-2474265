(function ($) {

    function csObjectSet( csObj ) {
        return ! csObjectNotSet( csObj );
    }

    function csObjectNotSet( object ) {

        if ( typeof object == 'undefined' || object == null || object == '' || object == '0' || object == '-1' ) {
            return true;
        }

        return false;

    }

    function csInitAjaxForm( csRef ) {

        var frmName;

        if ( csObjectNotSet(csRef) ) {
            frmName = "csAjxFrm";
        } else {
            frmName = csRef;
        }


        $("form." + frmName).ajaxForm( {
            beforeSerialize: csAjaxFormPost,
            success: csAjaxFormResponse,
            dataType: 'json',
            contentType: "application/x-www-form-urlencoded;charset=utf-8",
            timeout: 15000
        });

    }

    function csAjaxFormPost( form, options ) {
//        $(form).find('input[type=submit]').attr('disabled', true);
//        $(form).find("div.wdg").removeClass("vldMsg");
//        $(form).find("span.vldMsg").remove();
    }


    function csAjaxFormEnableButtons(form) {

        var uiDialog = $(form).parents(".ui-dialog");

        if ( csObjectSet( uiDialog ) && uiDialog.size() > 0 ) {
            uiDialog.find('input[type=submit]').removeAttr('disabled');
        } else {
            $('input[type=submit]').removeAttr('disabled');
        }

        $(form).children("input[name=btn]").remove();
    }


    function csAjaxFormResponse( data, status, xhr, form ) {

        var formClass = form.attr("class");

        csAjaxFormEnableButtons(form);


        if (  data == null || data == 'undefined' ) {

            var msg = '<span style="width:480px; margin:12px 0px 24px 24px;" class="vldMsg">';
            msg += 'Sorry, the server did not respond please try again.';
            msg += '</span>'
            $("form." + formClass).prepend(msg);

            return;

        }

        var goTo = data["go2"];

        if ( csObjectSet(goTo) ) {
            var baseUrl = getCsBaseUrl();
            window.location = baseUrl + "/" + goTo;
            return;
        }


        var c = 0;
        var rdr = data["rdr"];
        var uid = data["uid"];


        if ( ! csObjectNotSet(rdr) ) {
            csRedirect( rdr, uid );
            return;
        }


        for ( var i in data ) {
            var pfx = i.substring(0,3);
            if ( pfx == 'vld' ) {
                var id = i.substring(3);
                var d = $( "div#div" + id );
                d.addClass("vldMsg");
                var m = '<span class="vldMsg">' + data[i] + "</span>";
                d.append( m );
                c++;
            } else {
                $( "#" + i ).val( data[i] );
                $( "#" + i ).html( data[i] );
            }
        }


        if ( c > 0 ) {

            var msg = '<span style="width:480px; margin:12px 0px 24px 24px;" class="vldMsg">';
            msg += 'Sorry, we need a little more information before we ';
            msg += 'can proceed.  Please complete the fields outlined in red below';
            msg += '</span>'
            $("form." + formClass).prepend(msg);
            $("div.ui-layout-pane-center").animate({
                scrollTop: "0px"
            });
        }

        $("input#StatusProcessStage").attr("checked", false);
    }
	
	/* Categoty selections */
    $(document).ready(function() {
		
		$("#CourseCategoryGroupId").change(function(){
			
			
			
			var categoryid = $(this).val();
			if($.isNumeric(categoryid)){
				$(".region-filter-courses .panel-collapse").each(function(){
					$(this).find(".form-item-select").addClass("maximum");
				});
			}else{
				$(".region-filter-courses .panel-collapse").each(function(){
					$(this).find(".form-item-select").removeClass("maximum");
				});
			}
			

			$(".ui-select #CourseCategoryId").each(function(){
				$(this).attr("name","");
				if($(this).attr("class").split("_")[1]==categoryid){
					
					$(this).css("display","block");
					$(this).attr("name","CourseCategoryId");
				}else{
					$(this).css("display","none");
					$(this).attr("name","");
					$(".form-item-select").removeClass("max");
				}
			});
		});  
    });
}(jQuery));
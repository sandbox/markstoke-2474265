<?php

abstract class abstractFormat {

    protected $object;

    function __construct( abstractObject $object ) {
        $this->object = $object;

    }

    abstract function format();


}
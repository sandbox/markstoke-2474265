<?php


class blockSearchCategory
        extends abstractBlock {


    /**
     *
     * @return string
     */
    public static function getInfoText() {
        return 'CourseSales.com - a block with a control for searching by Course Category';

    }


    /**
     *
     * @return string
     */
    public static function getCacheType() {
        return DRUPAL_CACHE_PER_PAGE;

    }


    /**
     *
     * @return string
     */
    public static function getDelta() {
        return 'blockSearchCategory';

    }


    /**
     *
     * @return string
     */
    public function getTemplateName() {
        return 'search_category';

    }


    /**
     *
     * @return string
     */
    public function getTemplatePath() {
        return 'search_category';

    }


    /**
     *
     * @return array
     */
    public static function getTemplateVariables() {

        $result = array ( );

        $result[ 'text' ] = null;
        $result[ 'select_options' ] = null;
        $result[ 'objects' ] = null;

        return $result;

    }


    /**
     *
     * @param array $form
     */
    public function getConfigurationForm( array &$form ) {

        $this->addConfigurationTextField( $form );

    }


    /**
     *
     * @param array $edit
     */
    public function saveConfigurationForm( array $edit ) {

        $this->saveConfigurationVariable( $edit, self::blockText );

    }


    /**
     *
     * @param array $block
     */
    public function getContent( array &$block ) {

        global $csi;

        try {
            $csi->addSoapRequestActiveCategory();
            $csi->processRequest();
            $categories = $csi->getSoapDataActiveCategories();

            if ( !is_array( $categories ) ) {
                $categories = array ( );
            }

            $block[ 'title' ] = '';
            $block[ 'content' ] = theme( $this->buildTemplateName(),
                    array (
                'text' => variable_get( $this->getDelta() . self::blockText, '' ),
                'select_options' => $this->buildOptionSelect( $categories ),
                'objects' => $categories
                    ) );

// Catch
        } catch ( Exception $e ) {
            var_export( $e );
        }

    }

}
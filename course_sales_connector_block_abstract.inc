<?php


abstract class abstractBlock {
    /**
     *
     * @var string
     */

    const blockText = 'blockText';

    /**
     *
     * @var string
     */

    const blockFormListSize = 'blockFormListSize';

    /**
     *
     * @var array
     */
    protected $parameters;


    abstract public static function getInfoText();


    abstract public static function getCacheType();


    abstract public static function getDelta();


    abstract public static function getTemplateVariables();


    abstract public function getTemplateName();


    abstract public function getTemplatePath();


    abstract public function getConfigurationForm( array &$form );


    abstract public function saveConfigurationForm( array $edit );


    abstract public function getContent( array &$block );


    /**
     *
     * @return array
     */
    public static function getModules() {

        $result = array ( );

        $result[ ] = new blockCourseListSummary();
        $result[ ] = new blockCourseListSummaryCheapest();
        $result[ ] = new blockCourseListSummaryNext();
        $result[ ] = new blockCourseListDetail();
        $result[ ] = new blockFormDisplay();
        $result[ ] = new blockFormPosted();
        $result[ ] = new blockSearchLocation();
		$result[ ] = new blockCourseListDetailByLocation();
		$result[ ] = new blockLocationsMenu();
		$result[ ] = new blockSearchCoursesLocation();
        $result[ ] = new blockSearchCategory();
        $result[ ] = new blockSearchExtension();

        return $result;

    }


    /**
     *
     * @param string $delta
     * @return abstractBlock
     */
    public static function getModuleByDelta( $delta ) {

        switch ( $delta ) {

            case blockCourseListSummary::getDelta():
                return new blockCourseListSummary();

            case blockCourseListSummaryCheapest::getDelta():
                return new blockCourseListSummaryCheapest();

            case blockCourseListSummaryNext::getDelta():
                return new blockCourseListSummaryNext();

            case blockCourseListDetail::getDelta():
                return new blockCourseListDetail();

            case blockFormDisplay::getDelta():
                return new blockFormDisplay();

            case blockFormPosted::getDelta():
                return new blockFormPosted();

            case blockSearchLocation::getDelta():
                return new blockSearchLocation();
			
			case blockCourseListDetailByLocation::getDelta():
                return new blockCourseListDetailByLocation();
				
			case blockLocationsMenu::getDelta():
                return new blockLocationsMenu();
			
			case blockSearchCoursesLocation::getDelta():
                return new blockSearchCoursesLocation();

            case blockSearchCategory::getDelta():
                return new blockSearchCategory();

            case blockSearchExtension::getDelta():
                return new blockSearchExtension();
        }

    }


    /**
     *
     * @param array $blocks
     */
    public function getInfo( array &$blocks ) {
        $blocks[ $this->getDelta() ] = array (
            'info' => t( $this->getInfoText() ),
            'cache' => $this->getCacheType(),
        );

    }


    /**
     *
     * @param array $templates
     */
    public function getTemplate( array &$templates ) {

        $templates[ $this->buildTemplateName() ] = array (
            'variables' => $this->getTemplateVariables(),
            'template' => 'templates/' . constants::moduleName . '_block_' . $this->getTemplatePath(),
        );

    }


    /**
     *
     */
    public function getAjax( array &$data ) {

    }


    /**
     *
     * @return string
     */
    protected function buildTemplateName() {
        return constants::moduleName . '_block_' . $this->getTemplateName();

    }


    /**
     *
     * @param array $edit
     * @param string $name
     */
    public function saveConfigurationVariable( array $edit, $name ) {
        variable_set( $this->getDelta() . $name,
                $edit[ $this->getDelta() . $name ] );

    }


    /**
     *
     * @param array $edit
     * @param string $name
     */
    public function getConfigurationVariable( $name, $default ) {
        return variable_get( $this->getDelta() . $name, $default );

    }


    /**
     *
     * @param array $edit
     * @param string $name
     */
    public function getDebug() {
        watchdog( '',
                variable_get( $this->getDelta() . constants::csRequestDebug,
                        FALSE ) );
        return variable_get( $this->getDelta() . constants::csRequestDebug,
                FALSE );

    }


    /**
     *
     * @param array $form
     */
    protected function addConfigurationTextField( array &$form ) {

        $form[ $this->getDelta() . self::blockText ]
                = array (
            '#type' => 'textfield',
            '#title' => t( 'Text to show' ),
            '#size' => 40,
            '#default_value' => variable_get(
                    $this->getDelta() . self::blockText, '' )
        );

    }


    /**
     *
     */
    protected function setParameters() {

        if ( !empty( $this->parameters ) && is_array( $this->parameters ) ) {
            return;
        }

        parse_str( $_SERVER[ 'QUERY_STRING' ], $this->parameters );

        if ( !is_array( $_POST ) ) {
            return;
        }

        $this->parameters = array_merge( $this->parameters, $_POST );

    }


    /**
     *
     * @return array
     */
    public function getParameters() {

        if ( !is_array( $this->parameters ) ) {
            $this->parameters = array ( );
        }

        return $this->parameters;

    }


    /**
     *
     * @param string $parameter
     * @return boolean
     */
    protected function getParameterExists( $parameter ) {

        if ( !is_array( $this->parameters ) ) {
            return FALSE;
        }

        return array_key_exists( $parameter, $this->parameters );

    }


    /**
     *
     * @param string $parameter
     * @return string
     */
    protected function getParameter( $parameter ) {

        if ( !is_array( $this->parameters ) ) {
            return '';
        }

        if ( !array_key_exists( $parameter, $this->parameters ) ) {
            return '';
        }

        return $this->parameters[ $parameter ];

    }


    /**
     *
     * @param array $options
     * @return string
     */
    protected function buildOptionSelect( array $options ) {

        $result = '';

        foreach ( $options as $option ) {
            if ( !$option instanceof objectOption ) {
                continue;
            }
            $result .= '<option value="';
            $result .= $option->getId();
            $result .= '">';

            $depth = $option->getDepth();
            if ( $depth > 1 ) {
                $result .= str_repeat( '-', $depth - 1 ).' ';
            }

            $result .= $option->getName();
            $result .= '</option>';
        }

        return $result;

    }


    /**
     *
     * @param string $name
     */
    protected function stubOption() {

        $result = '<option value="1">' . $this->getDelta() . ' 1</option>';
        $result .= '<option value="2">' . $this->getDelta() . ' 2</option>';

        return $result;

    }

}
<?php


class blockCourseListDetail
        extends abstractBlockCourseList {


    
    /**
     *
     * @return string
     */
    public static function getInfoText() {
        return 'CourseSales.com - a block showing a listing of courses';

    }


    /**
     *
     * @return string
     */
    public static function getCacheType() {
        return DRUPAL_CACHE_PER_PAGE;

    }


    /**
     *
     * @return string
     */
    public static function getDelta() {
        return 'blockCourseDetail';

    }


    /**
     *
     * @return string
     */
    public function getTemplateName() {
        return 'course_list_detail';

    }


    /**
     *
     * @return string
     */
    public function getTemplatePath() {
        return 'course_list_detail';

    }

}
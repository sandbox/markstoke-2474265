<?php


abstract class abstractBlockCourseList
        extends abstractBlock {


    /**
     *
     * @var string
     */
    const blockPathForm = 'blockPathForm';


    /**
     *
     * @return string
     */
    public static function getCacheType() {
        return DRUPAL_CACHE_PER_PAGE;

    }


    /**
     *
     * @return array
     */
    public static function getTemplateVariables() {

        $result = array ( );

        $result[ 'text' ] = null;
        $result[ 'courses' ] = null;
        $result[ 'form' ] = null;
        $result[ 'documentExternalId' ] = null;
        $result[ 'parameters' ] = null;
        $result[ constants::dateFormat ] = null;
        $result[ self::blockPathForm ] = null;
        $result[ 'countTotal' ] = null;
        $result[ 'countPages' ] = null;

        return $result;

    }


    /**
     *
     * @param array $form
     */
    public function getConfigurationForm( array &$form ) {

        $this->addConfigurationTextField( $form );

        $form[ $this->getDelta() . self::blockFormListSize ]
                = array (
            '#type' => 'textfield',
            '#title' => t( 'Number of items to return' ),
            '#size' => 20,
            '#default_value' => variable_get(
                    $this->getDelta() . self::blockFormListSize, 10 ),
            '#description' => t( 'The maximum number of courses to show in the list.' )
        );

        $form[ $this->getDelta() . self::blockPathForm ]
                = array (
            '#type' => 'textfield',
            '#title' => t( 'Relative path when showing a Form' ),
            '#size' => 20,
            '#default_value' => variable_get(
                    $this->getDelta() . self::blockPathForm, '' ),
            '#description' => t( 'The path to a page to show the form, eg register.' )
        );

        $form[ $this->getDelta() . constants::dateFormat ]
                = array (
            '#type' => 'textfield',
            '#title' => t( 'Date Format' ),
            '#size' => 20,
            '#default_value' => variable_get(
                    $this->getDelta() . constants::dateFormat, 'j M Y' ),
            '#description' => t( 'A date format to use when showing dates, using <a target="_blank" href="http://php.net/manual/en/function.date.php">PHP formats</a>.' )
        );

    }


    /**
     *
     * @param array $edit
     */
    public function saveConfigurationForm( array $edit ) {
        $this->saveConfigurationVariable( $edit, self::blockText );
        $this->saveConfigurationVariable( $edit, self::blockFormListSize );
        $this->saveConfigurationVariable( $edit, constants::dateFormat );
        $this->saveConfigurationVariable( $edit, self::blockPathForm );

    }


    /**
     *
     * @param array $block
     */
    public function getContent( array &$block ) {

        global $csi;

        $this->setParameters();
        $csi->addCurrentRequest( $this->getParameters() );

        $this->setupRequestCourses();
        $this->setupRequestForm();

        $csi->processRequest();
        $countTotal = $csi->getSoapDataActiveCourseCount();

        $block[ 'title' ] = '';
        $block[ 'content' ] = theme( $this->buildTemplateName(),
                array (
            'text' => variable_get( $this->getDelta() . self::blockText, '' ),
            'courses' => $this->buildCourses(),
            'form' => $this->buildForm(),
            'documentExternalId' => $this->buildFormPostSucceeded(),
            'countTotal' => $countTotal,
            'countPages' => $this->buildCountPages( $countTotal ),
            'parameters' => $this->parameters,
            constants::dateFormat => variable_get(
                    $this->getDelta() . constants::dateFormat, 'j M Y' ),
            self::blockPathForm => variable_get(
                    $this->getDelta() . self::blockPathForm, '' )
                )
        );

    }


    /**
     *
     * @global courseSalesInterface $csi
     */
    protected function setupRequestCourses() {

        global $csi;

        $pageItems = variable_get(
                $this->getDelta() . self::blockFormListSize, 10 );

        $pageNum = null;
        if ( array_key_exists( constants::csRequestPage, $this->parameters ) ) {
            $pageNum = $this->parameters[ constants::csRequestPage ];
        }
        if ( empty( $pageNum ) ) {
            $pageNum = 1;
        }

        $csi->addSoapRequestActiveCourseList();
        $csi->setCurrentRequestPageItems( $pageNum, $pageItems );

    }


    /**
     *
     * @global courseSalesInterface $csi
     */
    protected function setupRequestCourseLocationNext() {

        global $csi;

        $pageItems = variable_get(
                $this->getDelta() . self::blockFormListSize, 10 );

        $pageNum = null;
        if ( array_key_exists( constants::csRequestPage, $this->parameters ) ) {
            $pageNum = $this->parameters[ constants::csRequestPage ];
        }
        if ( empty( $pageNum ) ) {
            $pageNum = 1;
        }

        $csi->addSoapRequestActiveCourseLocationNext();
        $csi->setCurrentRequestPageItems( $pageNum, $pageItems );

    }


    /**
     *
     * @global courseSalesInterface $csi
     */
    protected function setupRequestCourseLocationCheapest() {

        global $csi;

        $pageItems = variable_get(
                $this->getDelta() . self::blockFormListSize, 10 );

        $pageNum = null;
        if ( array_key_exists( constants::csRequestPage, $this->parameters ) ) {
            $pageNum = $this->parameters[ constants::csRequestPage ];
        }
        if ( empty( $pageNum ) ) {
            $pageNum = 1;
        }

        $csi->addSoapRequestActiveCourseLocationCheapest();
        $csi->setCurrentRequestPageItems( $pageNum, $pageItems );

    }


    /**
     *
     * @global courseSalesInterface $csi
     */
    protected function setupRequestForm() {

        global $csi;

        if ( $this->getQueryIsFormPost() ) {
            return $this->processFormPost();
        }

        if ( !$this->getQueryHasParametersForGetForm() ) {
            return;
        }

        $csi->addSoapRequestForm();

        $csi->addCurrentRequestItem( constants::csCourseDateId,
                $this->getParameter( constants::csCourseDateId ) );

        $csi->addCurrentRequestItem( constants::csStepId,
                $this->getParameter( constants::csStepId ) );

    }


    /**
     *
     * @return array
     */
    protected function buildForm() {

        global $csi;

        try {

            return $csi->getSoapDataForm();

// Catch
        } catch ( Exception $e ) {
            var_export( $e );
        }

    }


    /**
     *
     * @return array
     */
    protected function buildFormPostSucceeded() {

        global $csi;

        try {

            return $csi->getSoapPostFormSucceeded();

// Catch
        } catch ( Exception $e ) {
            var_export( $e );
        }

    }


    /**
     *
     */
    protected function processFormPost() {

        global $csi;

        try {

            $csi->addCurrentRequest( $this->getParameters() );
            $csi->addSoapPostForm();
            $csi->addSoapRequestPayment();

// Catch
        } catch ( Exception $e ) {
            var_export( $e );
        }

    }


    /**
     *
     * @param array $parms
     * @return boolean
     */
    protected function getQueryIsFormPost() {

        if ( !$this->getParameterExists( constants::csDocumentCourseDateId ) ) {
//            watchdog( '',
//                    'is not form post ' . constants::csDocumentCourseDateId );
            return FALSE;
        }

        if ( !$this->getParameterExists( constants::csDocumentFormId ) ) {
//            watchdog( '', 'is not form post ' . constants::csDocumentFormId );
            return FALSE;
        }

        if ( !$this->getParameterExists( constants::csDocumentSource ) ) {
//            watchdog( '', 'is not form post ' . constants::csDocumentSource );
            return FALSE;
        }
//        watchdog( '', 'is form post ' );
        return TRUE;

    }


    /**
     *
     * @param array $parms
     * @return boolean
     */
    protected function getQueryHasParametersForGetForm() {

        if ( !$this->getParameterExists( constants::csCourseDateId ) ) {
            return FALSE;
        }

        if ( !$this->getParameterExists( constants::csStepId ) ) {
            return FALSE;
        }

        return TRUE;

    }


    /**
     *
     * @return array
     */
    protected function buildCourses() {

        global $csi;

        $result = array ( );

        try {

            $courses = $csi->getSoapDataActiveCourseList();

            if ( !is_array( $courses ) ) {
                return $result;
            }

            foreach ( $courses as $course ) {
                if ( !is_array( $course ) ) {
                    continue;
                }
                $oCourse = new objectCourseDate( $course );
                $result[ $oCourse->getCourseDateId() ] = $oCourse;
            }

            $result[ 'form' ] = null;

            return $result;

// Catch
        } catch ( Exception $e ) {
            var_export( $e );
        }

    }


    /**
     *
     * @return array
     */
    protected function buildCoursesLocationCheapest() {

        global $csi;

        $result = array ( );

        try {

            $courses = $csi->getSoapDataActiveCourseLocationSummaryCheapest();

            if ( !is_array( $courses ) ) {
                return $result;
            }

            foreach ( $courses as $course ) {
                if ( !is_array( $course ) ) {
                    continue;
                }
                $oCourse = new objectCourseDate( $course );
                $result[ $oCourse->getCourseDateId() ] = $oCourse;
            }

            $result[ 'form' ] = null;

            return $result;

// Catch
        } catch ( Exception $e ) {
            var_export( $e );
        }

    }


    /**
     *
     * @return array
     */
    protected function buildCoursesLocationNext() {

        global $csi;

        $result = array ( );

        try {

            $courses = $csi->getSoapDataActiveCourseLocationSummaryNext();

            if ( !is_array( $courses ) ) {
                return $result;
            }

            foreach ( $courses as $course ) {
                if ( !is_array( $course ) ) {
                    continue;
                }
                $oCourse = new objectCourseDate( $course );
                $result[ $oCourse->getCourseDateId() ] = $oCourse;
            }

            $result[ 'form' ] = null;

            return $result;

// Catch
        } catch ( Exception $e ) {
            var_export( $e );
        }

    }


    /**
     *
     */
    public function getAjax( array &$data ) {

        global $csi;

        $this->setParameters();

        if ( $this->getQueryIsFormPost() ) {
            $this->processFormPost();
            $csi->processRequest();
            $data = array_merge( $data, $csi->getCurrentResponse() );
        }

        $this->setupRequestCourses();
        $csi->processRequest();
        $data = array_merge( $data, $csi->getCurrentResponse() );

    }


    /**
     *
     */
    protected function buildCountPages( $countTotal ) {

        $perPage = variable_get(
                $this->getDelta() . self::blockFormListSize, 10 );

        $pages = ceil( $countTotal / $perPage );

        return $pages;

    }


}
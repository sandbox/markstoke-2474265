<?php


class blockSearchExtension
        extends abstractBlock {
    /**
     *
     */

    const blockExtensionTreeId = 'blockExtensionTreeId';


    /**
     *
     * @return string
     */
    public static function getInfoText() {
        return 'CourseSales.com - a block with a control for searching by Option Extension';

    }


    /**
     *
     * @return string
     */
    public static function getCacheType() {
        return DRUPAL_CACHE_PER_PAGE;

    }


    /**
     *
     * @return string
     */
    public static function getDelta() {
        return 'blockSearchOptionExtension';

    }


    /**
     *
     * @return string
     */
    public function getTemplateName() {
        return 'search_extension';

    }


    /**
     *
     * @return string
     */
    public function getTemplatePath() {
        return 'search_extension';

    }


    /**
     *
     * @return array
     */
    public static function getTemplateVariables() {

        $result = array ( );

        $result[ 'text' ] = null;
        $result[ 'select_options' ] = null;
        $result[ 'objects' ] = null;

        return $result;

    }


    /**
     *
     * @param array $form
     */
    public function getConfigurationForm( array &$form ) {

        $this->addConfigurationTextField( $form );

        $form[ $this->getDelta() . self::blockExtensionTreeId ]
                = array (
            '#type' => 'textfield',
            '#title' => t( 'CourseSales.com Extension Tree Id' ),
            '#size' => 20,
            '#default_value' => variable_get(
                    $this->getDelta() . self::blockExtensionTreeId, '' )
        );

    }


    /**
     *
     * @param array $edit
     */
    public function saveConfigurationForm( array $edit ) {
        $this->saveConfigurationVariable( $edit, self::blockText );
        $this->saveConfigurationVariable( $edit, self::blockExtensionTreeId );

    }


    /**
     *
     * @param array $block
     */
    public function getContent( array &$block ) {

        global $csi;

        try {

            $extensionTreeId = variable_get(
                    $this->getDelta() . self::blockExtensionTreeId, '' );

            $csi->addSoapRequestOptionExtension( false, $extensionTreeId );
            $csi->processRequest();
            $options = $csi->getSoapDataOptionExtension( $extensionTreeId );

            if ( !is_array( $options ) ) {
                $options = array ( );
            }

            $block[ 'title' ] = '';
            $block[ 'content' ] = theme( $this->buildTemplateName(),
                    array (
                'text' => variable_get( $this->getDelta() . self::blockText, '' ),
                'select_options' => $this->buildOptionSelect( $options ),
                'objects' => $options
                    ) );

// Catch
        } catch ( Exception $e ) {
            var_export( $e );
        }

    }

}
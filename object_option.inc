<?php


class objectOption
        extends abstractObject {
    
////////////////////////////////////////////////////////////////////////////////
//
//      VARIABLES
//
////////////////////////////////////////////////////////////////////////////////

    /**
     *
     * @var integer
     */
    protected $id;

    /**
     *
     * @var string
     */
    protected $name;

    /**
     *
     * @var integer
     */
    protected $depth;

    /**
     *
     * @var integer
     */
    protected $formId;

    /**
     *
     * @var integer
     */
    protected $parentId;

    /**
     *
     * @var array
     */
    protected $content;

////////////////////////////////////////////////////////////////////////////////
//
//      CONSTANTS
//
////////////////////////////////////////////////////////////////////////////////

    /**
     * @static
     * @var integer
     */
    const soapOptionId = 1;

    /**
     * @static
     * @var integer
     */
    const soapOptionName = 2;

    /**
     * @static
     * @var integer
     */
    const soapOptionDepth = 3;

    /**
     * @static
     * @var integer
     */
    const soapOptionFormId = 4;

    /**
     * @static
     * @var integer
     */
    const soapOptionParentId = 5;

    /**
     * @static
     * @var integer
     */
    const soapOptionContent = 6;


    /**
     *
     * @param array $data
     */
    function __construct( array $data ) {

        $this->content = $data[ self::soapOptionContent ];
        $this->depth = $data[ self::soapOptionDepth ];
        $this->formId = $data[ self::soapOptionFormId ];
        $this->id = $data[ self::soapOptionId ];
        $this->name = $data[ self::soapOptionName ];
        $this->parentId = $data[ self::soapOptionParentId ];

    }


    /**
     *
     * @return integer
     */
    public function getId() {
        return $this->id;

    }


    /**
     *
     * @return string
     */
    public function getName() {
        return $this->name;

    }


    /**
     *
     * @return integer
     */
    public function getDepth() {
        return $this->depth;

    }


    /**
     *
     * @return integer
     */
    public function getFormId() {
        return $this->formId;

    }


    /**
     *
     * @return integer
     */
    public function getParentId() {
        return $this->parentId;

    }


    /**
     *
     * @return integer
     */
    public function getContent() {
        return $this->content;

    }

}